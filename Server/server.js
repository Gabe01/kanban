const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const projectRoutes = require('./Routes/Projects');

const app = express();

const mongooseURL = 'mongodb+srv://Gabe01:Secquone01!@testcluster.fj0jf.mongodb.net/Kanban?retryWrites=true&w=majority';

app.use(  
  cors({
    origin: "http://localhost:4200",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
  })
);

app.use(express.json());

app.use("/project", projectRoutes);

mongoose.connect(mongooseURL).then(() => {
  app.listen(3000,() => {
    console.log("Listening to port http://localhost:3000/");
  }) 
});