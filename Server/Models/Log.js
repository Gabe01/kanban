const { Schema, model, ObjectId } = require('mongoose');
const { ProjectModel, TaskModel } = require("./Project");

const Log = new Schema({
  action: {
    type: String,
    required: true
  },
  timestamp: {
    type: Date,
    required: false
  },
  task: {
    type: TaskModel,
    required: true
  },
  project: {
    type: ProjectModel,
  },
  uuid: ObjectId
});

const ProjectLog = new Schema({
  project: {
    type: ObjectId,
    required: true,
    unique: true,
  },
  logs: {
    type: [Log],
    required: true
  }
});

module.exports = {
  ProjectLog: model('ProjectLogs', ProjectLog)
};