const { Schema, model, ObjectId } = require('mongoose');

const TaskModel = new Schema({
  uuid: ObjectId,
  category: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  tags: {
    type: [String],
    required: true
  },
  priority: {
    type: String,
    required: true
  },  
  stage: {
    type: String,
    required: true
  },  
  duration: {
    type: Number,
    required: true
  },
  assigned: {
    type: String,
    required: true
  }
});

const ProjectModel = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  users: {
    type: [String],
    required: true
  },
  tasks: {
    type: [TaskModel],
  },
  uuid: ObjectId
});

module.exports = {
  Project: model('Projects', ProjectModel),
  TaskModel,
  ProjectModel
};