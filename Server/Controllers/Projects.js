const { mongoose } = require('mongoose');
const { Project } = require('../Models/Project');

exports.getShorts = async (req, res) => {
  const projects = await Project.find({});
  res.send(projects);       //TODO: Make this work with users
};

exports.newProject = async (req, res) => {
  const added = await new Project(req.body.project);
  added.uuid = new mongoose.Types.ObjectId();
  added.save();

  res.send(added);
}

exports.getProject = async (req, res) => {
  const proj = await Project.findOne({ uuid: req.params["id"]});
  res.send(proj);
}

exports.newTask = async (req, res) => {
  const proj = await Project.findOne({ uuid: req.params["id"]});
  proj.tasks.push({...req.body.task, uuid: new mongoose.Types.ObjectId()});

  await proj.save();

  res.send({});
}

exports.deleteTask = async (req, res) => {
  const proj = await Project.findOne({ uuid: req.params["id"]});
  proj.tasks = proj.tasks.filter((task) => task.uuid != req.body.task.uuid);
  proj.save();

  res.send({});
}

exports.progressTask = async (req, res) => {
  const proj = await Project.findOne({ uuid: req.params["id"]});

  proj.tasks.forEach((task) => {
    if(task.uuid == req.body.task.uuid){
      task.stage = NextStage(task.stage);
    }
  });

  proj.save();

  res.send({});
}

function NextStage(stage) {
  if(stage == 'todo')
    return 'in-progress'
  if(stage == 'in-progress')
    return 'review'
  if(stage == 'review')
    return 'done'

  return 'NULL';
}