const { getShorts, newProject, getProject, newTask, progressTask, deleteTask } = require('../Controllers/Projects');
const { LogHistory } = require('../history');

const express = require('express');
const route = express.Router();

route.get("/shorts", getShorts);
route.post("/new", newProject);
route.get("/:id", getProject);
route.post("/:id/add", LogHistory, newTask);
route.post("/:id/progress", LogHistory, progressTask);
route.post("/:id/delete", LogHistory, deleteTask);

module.exports = route;