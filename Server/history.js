const { mongoose } = require('mongoose');
const { ProjectLog } = require("./Models/Log");

exports.LogHistory = async (req, res, next) => {
  //Get Project id from the project prop in the body OR from the url params 
  let projectID = req.params["id"];
  let proj = await ProjectLog.findOne({project: projectID});

  console.log(req.params);

  //Create a Project if it doesn't exist
  if(proj == undefined) {
    proj = new ProjectLog({
      project: new mongoose.Types.ObjectId(projectID),
      uuid: new mongoose.Types.ObjectId(),
      logs: []
    });
  }

  proj.logs.push({
    action: req.body.action,
    timestamp: new Date(),
    task: req.body.task,
    project: req.body.project
  });

  await proj.save();

  next();
}