import { Project } from './Project'
import { Task } from './Tasks'

export type PostRequest = {
  action: string
  project: Project | undefined,
  task: Task | undefined
}