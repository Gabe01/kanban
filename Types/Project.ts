import { Task } from "./Tasks";
import { User } from "./User";

export type ProjectShort = {
  uuid: string
  name: string,
  description: string,
  users: User[]       //TODO: This should probably be the user type 
};

export type Project = ProjectShort & {
  tasks: Task[]
}