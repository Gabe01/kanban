import { User } from "./User";

export enum TaskCategory {
  Development = "DEVELOPMENT",
  Design = "DESIGN",
  Marketing = "MARKETING",
  Management = "MANAGEMENT"
};

export enum TaskPriority {
  High = "High",
  Mid = "Med",
  Low = "Low"
};

export type Task = {
  uuid: ""
  category: TaskCategory,
  name: string,
  description: string,
  tags: string[],
  priority: TaskPriority,
  duration: number,
  stage: string,
  assigned: string
};

export const EMPTY_TASK : Task = {
  uuid: '',
  category: TaskCategory.Design,
  name: '',
  description: '',
  tags: [],
  priority: TaskPriority.High,
  duration: 0,
  stage: "todo",
  assigned: ""
};

//TODO: This needs to be a global utils function or something
export function NextStage(stage: string) : string {
  if(stage == 'todo')
    return 'in-progress'
  if(stage == 'in-progress')
    return 'review'
  if(stage == 'review')
    return 'done'

  return 'NULL';
}