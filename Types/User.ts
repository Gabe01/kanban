import { TaskCategory } from "./Tasks";

export type User = {
  name: string,
  category: TaskCategory
};