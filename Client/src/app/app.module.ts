import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardPageComponent } from './Pages/dashboard-page/dashboard-page.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { ProjectCardComponent } from './Components/project-card/project-card.component';
import { StoreModule } from '@ngrx/store';
import { ProjectPageComponent } from './Pages/project-page/project-page.component';
import { EffectsModule } from '@ngrx/effects';
import { DatabaseService } from './Services/database.service';
import { DashboardEffects } from './Store/Dashboard/dashboard.effects';

import { DashboardReducer, DashboardStoreState } from './Store/Dashboard/dashboard.reducer';
import { TaskboardReducer, TaskboardStoreState } from './Store/Taskboard/taskboard.reducer';
import { TaskboardEffects } from './Store/Taskboard/taskboard.effects';
import { TaskCardComponent } from './Components/task-card/task-card.component';
import { CommonModule } from '@angular/common';
import { TaskStageComponent } from './Components/task-stage/task-stage.component';
import { TagComponent } from './Components/tag/tag.component';
import TagSystem from './Services/tagging.service';

export const StoreModuleReducers = {
  DashStore: DashboardReducer,
  TaskStore: TaskboardReducer,
}

export type StoreModuleType = {
  DashStore: DashboardStoreState,
  TaskStore: TaskboardStoreState
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardPageComponent,
    NavbarComponent,
    ProjectCardComponent,
    ProjectPageComponent,
    TaskCardComponent,
    TaskStageComponent,
    TagComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(StoreModuleReducers, {}),
    EffectsModule.forRoot([DashboardEffects, TaskboardEffects])
  ],
  providers: [
    DatabaseService,
    TagSystem
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
