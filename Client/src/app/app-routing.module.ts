import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPageComponent } from './Pages/dashboard-page/dashboard-page.component';
import { ProjectPageComponent } from './Pages/project-page/project-page.component';

const routes: Routes = [
  { path: "", component: DashboardPageComponent },
  { path: "project/:id", component: ProjectPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
