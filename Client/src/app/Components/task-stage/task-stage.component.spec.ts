import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskStageComponent } from './task-stage.component';

describe('TaskStageComponent', () => {
  let component: TaskStageComponent;
  let fixture: ComponentFixture<TaskStageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TaskStageComponent]
    });
    fixture = TestBed.createComponent(TaskStageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
