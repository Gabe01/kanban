import { Component, Input } from '@angular/core';
import { Task } from '../../../../../Types/Tasks';

@Component({
  selector: 'TaskStage',
  templateUrl: './task-stage.component.html',
  styleUrls: ['./task-stage.component.scss']
})
export class TaskStageComponent {
  @Input() stageName: string = "";
  @Input() tasks: Task[] = [];

  formOpen = false;
}
