import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addTask, deleteTask, progressTask } from 'src/app/Store/Taskboard/taskboard.actions';
import { StoreModuleType } from 'src/app/app.module';
import { EMPTY_TASK, Task, TaskCategory, TaskPriority } from '../../../../../Types/Tasks';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'TaskCard',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss']
})
export class TaskCardComponent {
  @Input() render : boolean = false;
  @Input() task : Task = EMPTY_TASK;
  @Output() cancelForm = new EventEmitter<string>();
  
  projectID: string = "";
  form : FormGroup = new FormGroup({
    category:     new FormControl('', Validators.required),
    name:         new FormControl('', Validators.required),
    tags:         new FormControl('', Validators.required),
    description:  new FormControl('', Validators.required),
    priority:     new FormControl('', Validators.required),
    duration:     new FormControl('', Validators.required),
  });

  constructor(private store: Store<StoreModuleType>, private route: ActivatedRoute) {
    route.params.subscribe((params) => {
      this.projectID = params["id"];
    });
  }

  Cancel() {
    this.cancelForm.emit();
  }

  Submit() {
    if(!this.form.valid){
      return;
    }

    this.store.dispatch(addTask({
      task: {
        ...this.form.value, 
        tags: this.form.value.tags.split(' '),
        stage: 'todo',
        assigned: 'Gabe'
      }, 
      project: this.projectID
    }));

    this.cancelForm.emit();
    this.form.reset();
  }

  onChange(value: any) {
    if(value.target.value == "progress")
      this.store.dispatch(progressTask({ task: this.task, project: this.projectID}));
    if(value.target.value == "delete")
      this.store.dispatch(deleteTask({ task: this.task, project: this.projectID}));
  }
}
