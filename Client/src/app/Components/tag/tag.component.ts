import { Component, Input } from '@angular/core';
import TagSystem from 'src/app/Services/tagging.service';

@Component({
  selector: 'Tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent {
  @Input() name : string = "";

  constructor(private tagSystem: TagSystem) {}

  getColor() {
    return this.tagSystem.TagColor(this.name);
  }
}
