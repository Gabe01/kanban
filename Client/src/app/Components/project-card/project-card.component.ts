import { Component, Input } from '@angular/core';
import { ProjectShort } from '../../../../../Types/Project';

@Component({
  selector: 'ProjectCard',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class ProjectCardComponent {
  @Input() project! : ProjectShort;
}
