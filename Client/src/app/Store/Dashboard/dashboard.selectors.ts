import { createFeatureSelector, createSelector } from "@ngrx/store";
import { DashboardStoreState } from "./dashboard.reducer";

export const getDashboardState = createFeatureSelector<DashboardStoreState>('DashStore');

export const getProjects = createSelector(
  getDashboardState,
  (state: DashboardStoreState) => state.projects
);