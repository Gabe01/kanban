import { createReducer, on } from "@ngrx/store";
import { createProject, createProjectSuccess, loadShortSuccess, loadShorts } from "./dashboard.actions";
import { ProjectShort } from '../../../../../Types/Project';

export type DashboardStoreState = {
  projects: ProjectShort[]
}

const dashboardInit : DashboardStoreState = {
  projects: []
};

export const DashboardReducer = createReducer(
  dashboardInit,
  on(createProject, (state, project) => { 
    return state; 
  }),
  on(loadShorts, (state) => {
    return state;
  }),
  on(loadShortSuccess, (state, action) => { 
    return { projects: action.payload }; 
  }),
  on(createProjectSuccess, (state, action) => {
    return {...state, projects: [...state.projects, action.payload]};
  })
);