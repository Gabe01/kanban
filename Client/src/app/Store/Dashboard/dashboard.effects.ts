import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { DatabaseService } from "../../Services/database.service";
import { EMPTY, catchError, exhaustMap, map } from "rxjs";
import * as DashboardActions from "./dashboard.actions" 

@Injectable()
export class DashboardEffects {
  
  loadShorts = createEffect(() => this.actions.pipe(
    ofType(DashboardActions.loadShorts),
    exhaustMap(() => this.database.getProjectShorts()
      .pipe(
        map(shorts => ({ type: '[Dashboard] Loaded Shorts Success', payload: shorts })),
        catchError(() => EMPTY)
      )
    )
  ));
  
  addShort = createEffect(() => this.actions.pipe(
    ofType(DashboardActions.createProject),
    exhaustMap((short) => this.database.createProject(short)
      .pipe(
        map((short) => ({type: '[Dashboard] Create Project Success', payload: short})),
        catchError(() => EMPTY)
      )
    )
  ));

  constructor(
    private actions: Actions,
    private database: DatabaseService
  ){}
}