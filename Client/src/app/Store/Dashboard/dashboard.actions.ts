import { createAction, props } from "@ngrx/store";
import { ProjectShort } from "../../../../../Types/Project";

export const createProject = createAction(
  '[Dashboard] Create', props<ProjectShort>()
);

export const loadShorts = createAction(
  '[Dashboard] Load'
);

export const loadShortSuccess = createAction(
  '[Dashboard] Loaded Shorts Success', props<any>()
);

export const createProjectSuccess = createAction(
  '[Dashboard] Create Project Success', props<any>()
);