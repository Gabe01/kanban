import { Injectable } from "@angular/core";
import { DatabaseService } from "../../Services/database.service";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as TaskboardActions from "./taskboard.actions";
import { EMPTY, catchError, exhaustAll, exhaustMap, map } from "rxjs";

@Injectable()
export class TaskboardEffects {

  project = createEffect(() => this.actions.pipe(
    ofType(TaskboardActions.getProject),
    exhaustMap((action) => this.database.getProject(action.uuid)
      .pipe(
        map((project) => ({ type: '[Project] Get Project Success', project })),
        catchError(() => EMPTY)
      )
    )
  ));

  newTask = createEffect(() => this.actions.pipe(
    ofType(TaskboardActions.addTask),
    exhaustMap((action) => this.database.addTask(action.task, action.project)
      .pipe(
        map((task) => ({type:'[Project] Add Task', task})),
        catchError(() => EMPTY)
      )
    )
  ));

  progress = createEffect(() => this.actions.pipe(
    ofType(TaskboardActions.progressTask),
    exhaustMap((action) => this.database.progressTask(action.task, action.project)
      .pipe(
        map((task) => ({type:'[Project] Progress Task', task})),
        catchError(() => EMPTY)
      )
    )
  ));
  
  deleteTask = createEffect(() => this.actions.pipe(
    ofType(TaskboardActions.deleteTask),
    exhaustMap((action) => this.database.deleteTask(action.task, action.project)
      .pipe(
        map((task) => ({type:'[Project] Delete Task Success', task})),
        catchError(() => EMPTY)
      )
    )
  ));

  constructor(
    private actions: Actions,
    private database: DatabaseService
  ){}
}