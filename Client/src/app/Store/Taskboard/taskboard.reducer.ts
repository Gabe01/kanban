import { createReducer, on } from "@ngrx/store";
import { Project } from "../../../../../Types/Project";
import { addTask, deleteTask, deleteTaskSuccess, getProject, getProjectSuccess, progressTask, progressTaskSuccess } from "./taskboard.actions";
import { NextStage, Task } from "../../../../../Types/Tasks";

export type TaskboardStoreState = {
  project: Project
};

const taskboardInit : TaskboardStoreState = {
  project: {
    uuid: "____",
    name: "NULL",
    description: "NULL",
    users: [],
    tasks: []
  }
}

export const TaskboardReducer = createReducer(
  taskboardInit,
  on(getProject, (state) => {
    return state;
  }),
  on(getProjectSuccess, (state, payload) => {
    return { project: payload.project };
  }),
  on(addTask, (state, req) => {
    return {
      project: {
        ...state.project,
        tasks: [...state.project.tasks, req.task]
      }
    }
  }),
  on(progressTask, (state, payload) => {
    let newTasks = [...state.project.tasks];

    newTasks = newTasks.map((task: Task) => {
      if(task.uuid == payload.task.uuid)
        return {
          ...task,
          stage: NextStage(task.stage)
        }
      else
        return task;
    })

    return {
      project: {
        ...state.project,
        tasks: newTasks
      }
    }
  }),
  on(deleteTask, (state, payload) => {
    return {
      ...state,
      project: {
        ...state.project,
        tasks: state.project.tasks.filter((task) => task.uuid != payload.task.uuid)
      }
    }
  }),
  on(progressTaskSuccess, (state, payload) => {
    console.log("Progress Success");
    return state;
  }),
  on(deleteTaskSuccess, (state, payload) => {
    console.log("Delete Task Success")
    return state;
  })
);