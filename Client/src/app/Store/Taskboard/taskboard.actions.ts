import { createAction, props } from "@ngrx/store";
import { Task } from "../../../../../Types/Tasks";

export const getProject = createAction(
  '[Project] Get Project', props<{ uuid: string }>()
);

export const getProjectSuccess = createAction(
  '[Project] Get Project Success', props<any>()
);

export const addTask = createAction(
  '[Project] Add Project', props<{task: Task, project: string}>()
);

export const addTaskSuccess = createAction(
  '[Project] Add Task', props<any>()
);

export const deleteTask = createAction(
  '[Project] Delete Task', props<{task: Task, project: string}>()
);

export const deleteTaskSuccess = createAction(
  '[Project] Delete Task Success', props<any>()
);


export const progressTask = createAction(
  '[Project] Progress Task', props<{task: Task, project: string}>()
);

export const progressTaskSuccess = createAction(
  '[Project] Progress Task Success', props<any>()
);
