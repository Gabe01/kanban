import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ProjectShort } from "../../../../Types/Project";
import { Task } from "../../../../Types/Tasks";

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  constructor(private http: HttpClient){}

  getProjectShorts(){
    return this.http.get("http://localhost:3000/project/shorts");
  }

  getProject(uuid: String){
    return this.http.get(`http://localhost:3000/project/${uuid}`);
  }

  createProject(project: ProjectShort) {
    return this.http.post("http://localhost:3000/project/new", {
      action: "Create Project",
      project,
      task: undefined
    });
  }

  progressTask(task: Task, uuid: string) {
    return this.http.post(`http://localhost:3000/project/${uuid}/progress`, {
      action: "Progress Task",
      project: undefined,
      task
    });
  }

  addTask(task: Task, uuid: string){
    return this.http.post(`http://localhost:3000/project/${uuid}/add`, {
      action: "Add Task",
      project: undefined,
      task
    });
  }

  deleteTask(task: Task, uuid: string){
    return this.http.post(`http://localhost:3000/project/${uuid}/delete`, {
      action: "Delete Task",
      project: undefined,
      task
    });
  }
}