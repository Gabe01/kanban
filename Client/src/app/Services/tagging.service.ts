import { Injectable } from "@angular/core";
import { hslToHex, hslToRgb, percievedBrightness } from "./colors";

@Injectable()
export default class TagSystem {
  colors : string[] = [
    '#E73C3C',
    '#E76F3C',
    '#FFB142',
    '#EED200',
    '#B3DB00',
    '#5BA40C',
    '#00BECA',
    '#3498DB',
    '#6975DE',
    '#9A5EE7',
    '#C336E7',
    '#DB34AC',
    '#962727',
    '#964827',
    '#A6732B',
    '#9B8900',
    '#5E7300',
    '#3B6B08',
    '#007C83',
    '#22638F',
    '#444C91',
    '#643D96',
    '#7F2396',
    '#8F2270'
  ]; 

  //Hash string into 0-1 value
  private textHash(text: string) : number {
    var hash = 0,
    i, chr;
    if (text.length === 0) return hash;
    for (i = 0; i < text.length; i++) {
      chr = text.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash % 360;
  }

  //Get Hex from tag
  public TagColor = (text: string) => {
    let hash = this.textHash(text);
    let rgb = hslToRgb(hash, 100, 50);
    let brigthness = 255 - percievedBrightness(rgb.r, rgb.g, rgb.b);
    let lightnessAdjust = brigthness / 255;

    return hslToHex(hash, 70 + (100*lightnessAdjust),60 - (80*lightnessAdjust));
    //return this.colors[hash%12];
  }
}