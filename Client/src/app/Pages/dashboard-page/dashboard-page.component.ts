import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { DashboardStoreState } from 'src/app/Store/Dashboard/dashboard.reducer';
import { getProjects } from 'src/app/Store/Dashboard/dashboard.selectors';
import { createProject, loadShorts } from 'src/app/Store/Dashboard/dashboard.actions';
import { ProjectShort } from '../../../../../Types/Project';
import {v4 as uuidv4} from 'uuid';
import { StoreModuleType } from 'src/app/app.module';

//TODO: Rename to Dashboard
@Component({
  selector: 'DashboardPage',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent {
  projects : ProjectShort[] = [];

  constructor(private store: Store<StoreModuleType>) {
    store.select(getProjects).subscribe((state) => this.projects = state );
  }

  ngOnInit() {
    this.store.dispatch(loadShorts());
  }

  addProject() {
    this.store.dispatch(createProject({ uuid: uuidv4(), name: "Test", description:"Lorum Ipsum", users:[]}));
  }
}
