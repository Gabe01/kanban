import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../../../../../Types/Project';
import { Store } from '@ngrx/store';
import { StoreModuleType } from 'src/app/app.module';
import { getProject, progressTask } from 'src/app/Store/Taskboard/taskboard.actions';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss']
})
export class ProjectPageComponent {
  projectID : string = "";
  project : Project = {
    uuid: "____",
    name: "NULL",
    description: "NULL",
    users: [],
    tasks: []
  };

  constructor(route: ActivatedRoute, private store: Store<StoreModuleType>) {
    route.params.subscribe((params) => {
      this.projectID = params["id"];
      store.dispatch(getProject({ uuid: params["id"] }));
    });
  }

  ngOnInit() {
    this.store.select((state) => state.TaskStore.project).subscribe(project => {
      this.project = project;
    });
  }

  StageTasks = (stage: string) => this.project.tasks.filter((task) => task.stage == stage);
}
